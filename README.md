# st: Simple Terminal

st is a simple terminal implementation for X

## Patches

- [anysize](https://st.suckless.org/patches/anysize/st-anysize-0.8.1.diff).
- [alpha](https://st.suckless.org/patches/alpha/st-alpha-0.8.2.diff).
- [st-scrollback](https://st.suckless.org/patches/scrollback/st-scrollback-20201205-4ef0cbd.diff).
- [st-scrollback-mouse](https://st.suckless.org/patches/scrollback/st-scrollback-mouse-20191024-a2c479c.diff).
- [font2](https://st.suckless.org/patches/font2/st-font2-20190416-ba72400.diff).
